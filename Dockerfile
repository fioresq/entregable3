# Imagen docker base inicial
FROM node:latest

# crear directorio de trabajo del contenedor docker
WORKDIR /docker-dir-apitechu

# copiar archivos del proyecto en el directorio del trabajo docker
ADD . /docker-dir-apitechu

# instalar dependencias produccion del proyecto
# RUN npm install --only=production

# puerto donde exponemos contenedor
EXPOSE 3001

# comando para lanzar la app
CMD ["npm", "run", "prod"]
