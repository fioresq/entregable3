require('dotenv').config();
const cors = require('cors');
const express = require('express');
const app = express();
const port = process.env.PORT ||3000; // process.env
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu19db/collections/';
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;
const userFile = require('./users.json');
const body_parser = require('body-parser');
const request_json = require('request-json');

app.listen (port, function(){
  console.log('Node js escuchando desde el puerto:'+ port);
});

app.use(body_parser.json());
app.use(cors());
app.options('*', cors());

//Method POST login con mLab
app.post(URL_BASE + "login",
  function (req, res){
    var email= req.body.email;
    var pass= req.body.password;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var queryStringpass='q={"password":' + pass + '}&';
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('account?'+ queryStringEmail+apikey_mlab ,
    function(error, respuestaMLab , body) {
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session={"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(URL_mLab+'?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab);
            clienteMlab.put('account?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(login),
             function(errorP, respuestaMLabP, bodyP) {
              res.send(body[0]);
            });
          }
          else {
            console.log("contraseña incorrecta");
            res.send({"msg":"login incorrecto"});
          }
      }else{
        console.log("Email Incorrecto");
        res.send({"msg": "login Incorrecto"});
      }
    });
});

//Method POST logout con Mlab
app.post(URL_BASE + "logout",
  function (req, res){
    var email= req.body.email;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('account?'+ queryStringEmail+apikey_mlab ,
    function(error, respuestaMLab , body) {
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
            console.log("logout Correcto");
            var session={"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('account?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
             function(errorP, respuestaMLabP, bodyP) {

              res.send({"msg": "logout Exitoso"});
            });
      }else{
        console.log("Error en logout");
        res.send({"msg": "Error en logout"});
      }
    });
	});
